<?php namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;

class CalculateController extends Controller
{

	private $bills = array(10, 20, 50, 100);
	private $money_left;
	private $cash = array();

	function __construct() {
		rsort($this->bills);
	}


	public function index()
	{
		return view('calculate.index');
	}

	public function getBills($withdrawAmount) {
		$this->cash = array();
		$this->money_left = $withdrawAmount;
		while ($this->money_left > 0) {
			if ($this->money_left < min($this->bills)) {
				throw new WithdrawException('This amount cannot be paid.');
			}
			$bill = $this->configureBills();
			$this->cash[] = $bill;
			$this->money_left -= $bill;
		}
		return array_count_values($this->cash);
	}

	private function configureBills() {
		foreach ($this->bills as $bill) {
			$division = $this->money_left / $bill;
			$rest = $this->money_left % $bill;
			if ( ($division >= 1) && ( $rest > (min($this->bills)+1) || ($rest === min($this->bills)) || ($rest === 0) ) ) {
				return $bill;
			}
		}
		return min($this->bills);
	}


	public function calculate()
	{
		$value = Input::get('value');

		if ($value) {
			if ($value < 0) {
				echo '{"error":"InvalidArgumentException"}';

			}
			elseif (($value % 10) == 0) {
				print_r(json_encode($this->getBills($value)));
			}
			else {
				echo '{"error":"BillUnavaiableException"}';
			}
		}
		else {
			echo '{"0":0}';
		}
	}
}
