<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Teste do Caixa Eletrônico</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>

    <div class="container">
        <h1>Teste do Caixa Eletrônico</h1>

        @yield('content')

    </div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

    <script>
    $("#form-login").submit(function(){

      var selector = $(this);
      $.ajax({
         url: selector.attr("action"),
         type: "post",
         data: selector.serialize(),
         dataType: "json",
      }).done(function(data){
         console.log(data);
         if(data.status == "failed"){
           alert("error");
         }else{
            alert("success");
         }
      });

      return false;
    });
</body>
</html>