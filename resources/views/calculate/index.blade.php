<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Teste do Caixa Eletrônico</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

    <div class="container">
        <h1>Teste do Caixa Eletrônico</h1>

        <form id="calculate">
        	<input type="hidden" name="_token" value="{{ csrf_token() }}">
        	<input class="form-control" type="text" name="value" />
        	<input class="btn btn-primary" type="submit" value="Sacar" />
        </form>


    </div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script>

        $(document).ready(function() {

            $("#calculate").submit(function(){

		    var selector = $(this);

		    $.ajax({
	             url: '/calculate',
	             type: "post",
	            data: selector.serialize(),
	            dataType: "json",
	         }).done(function(data){
	            if(data.status == "failed"){
	                alert("Erro");
	             }
	             else{
	                alert(JSON.stringify(data));
	            }
	        });
	        return false;
		    });
		});
	</script>
</body>
</html>

